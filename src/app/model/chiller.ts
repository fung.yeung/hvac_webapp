export class Chiller {
    id?: string;
    name?: string;
    selected?: boolean;
    size?: number;
    dropOffPercent?: number;
    operationYears?: number;
}